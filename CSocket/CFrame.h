//
//  CFrame.h
//  CSocket
//
//  Created by Наиль  on 28.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

typedef NS_ENUM(NSInteger, CFrameType)
{
    CFrameType_TEXT = 0,
    CFrameType_BINARY,
    CFrameType_PING,
    CFrameType_PONG
};

@interface CFrame : NSObject

@property (nonatomic, strong) NSMutableData *data;

@property (nonatomic, assign) float offset;

@property (nonatomic, strong) CFrame *prevFrame;

@property (nonatomic, assign, getter=isLast) BOOL last;

@property (nonatomic, assign) BOOL filled;

@property (nonatomic, assign) uint64_t payload_lenght;

@property (nonatomic, assign) CFrameType frameType;

- (NSData *) componentsJoined;

@end
