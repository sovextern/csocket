//
//  c_socket_main.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#include <netdb.h>
#include <netinet/in.h>
#include "cerror.h"

class csocket_main
{
public:
    csocket_main(const char *host);
    bool connectToSocket();
    bool sendToSocket(const void* data);
    cerror* lastError();
private:
    
    void *get_in_addr(struct sockaddr *sa);
    
    const char *host;
    int socket_desc;
    cerror *lError;
};

