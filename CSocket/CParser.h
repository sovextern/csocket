//
//  CParser.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

typedef NS_ENUM(NSInteger, RFCOpcode)
{
    RFCOpcode_TEXT = 0x1,
    RFCOpcode_BINARY = 0x2,
    RFCOpCode_CONNECTION_CLOSE = 0x8,
    RFCOpCode_PING = 0x9,
    RFCOpCode_PONG = 0xA
};

@class CFrame;

@protocol CParserDelegate <NSObject>

- (void) didCompleteParseFrame:(CFrame *)frame;

@end

@interface CParser : NSObject

@property (nonatomic, weak) id<CParserDelegate> delegate;

- (NSData *) parseDataFromJSONDictionary:(NSDictionary *)dict;

- (NSData *) parseDataToRFCStandartFromData:(NSData *)data withCode:(RFCOpcode)code;

- (void) parseDataFromRFSStandart:(NSData *)data;

@end
