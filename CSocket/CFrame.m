
//
//  CFrame.m
//  CSocket
//
//  Created by Наиль  on 28.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFrame.h"


@implementation CFrame

- (instancetype) init
{
    if ( (self = [super init]) == nil )
    {
        return nil;
    }
    self.data = [NSMutableData new];
    return self;
}

- (NSData *) componentsJoined
{
    if (!self.prevFrame)
    {
        return self.data;
    }
    
    NSMutableData *data = nil;
    
    CFrame *frame = self;
    while (frame)
    {
       if (!data)
       {
           data = [NSMutableData dataWithData:frame.data];
       }
       else
       {
           NSMutableData *paggin_data = data;
           
           data = [NSMutableData dataWithData:frame.data];
           [data appendData:paggin_data];
       }
        
       frame = frame.prevFrame;
    }
    
    
    return [data copy];
}

@end
