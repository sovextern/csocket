//
//  c_socket_main.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#include "csocket_main.h"

#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <arpa/inet.h>


csocket_main::csocket_main(const char *host)
{
    this->socket_desc = -1;
    this->host = host;
}

bool csocket_main::connectToSocket()
{
    int sock_desc = 0;
    struct addrinfo *p, hints, *servinfo;
    char s[INET6_ADDRSTRLEN];

    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    
    int status = getaddrinfo(this->host,"3001", &hints, &servinfo);
    
    if (status != 0)
    {
        char *error_desc = new char();
        sprintf(error_desc, "getaddrinfo: %s\n",gai_strerror(status));
        this->lError = new cerror(error_desc);
        return false;
    }
     for(p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((sock_desc = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("Client: socket");
            continue;
        }
        
        if (connect(sock_desc, p->ai_addr, p->ai_addrlen) == -1) {
    
            perror("Client: connect");
            continue;
        }
        
        break;
    }
    
    if ( p == NULL )
    {
        const char *error_desc = "Failed to connect at socket with status:";
        this->lError = new cerror(errno,error_desc);
        return false;
    }
    
    
    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
              s, sizeof s);
    printf("client: connecting to %s\n", s);
    freeaddrinfo(servinfo);
    
    char message[255] = "";
    
    char *request = new char();
    sprintf(request, "GET /ws HTTP/1.1 \n Host:%s:3001 \n Upgrade: websocket \n Connection: Upgrade",this->host);
    printf("%s",request);
    size_t res = send(sock_desc,request,strlen(request),0);

    
    if (res == 0)
    {
        printf("FILED BICH OF MOTHERFUCKER");
    }
    
    return true;
}

bool csocket_main::sendToSocket(const void *data)
{
    if (data == NULL)
    {
        return false;
    }
    
    size_t sz = send(socket_desc, data, sizeof(data), 0);
    
    if (sz == 0)
    {
        return false;
    }
    
    return true;
}

void* csocket_main::get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

cerror* csocket_main::lastError()
{
    return this->lError;
}



