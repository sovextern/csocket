//
//  cerror.cpp
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#include "cerror.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>


cerror::cerror(int error_code,const char *error_desc)
{
    this->error_code = error_code;
    this->error_desc = error_desc;
}

cerror::cerror(int error_code)
{
    this->error_code = error_code;
    this->error_desc = nullptr;
}

cerror::cerror(const char *error_desc)
{
    this->error_desc = error_desc;
    this->error_code = -1;
}

const char* cerror::formmatedError()
{
    char *s = new char();
    if (error_code != -1 && error_desc != nullptr)
    {
        sprintf(s, "%s,%s",error_desc,strerror(error_code));
        return s;
    }
    
    if (error_code != -1)
    {
        s = strerror(error_code);
    }
    
    if (error_desc != nullptr)
    {
        sprintf(s, "%s", error_desc);
    }
    
    return s;
}
