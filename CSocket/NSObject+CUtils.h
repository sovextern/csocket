//
//  NSObject+CUtils.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject(CUtils)

- (id) objectAs:(Class)cls;

@end
