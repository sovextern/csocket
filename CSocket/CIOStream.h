//
//  CStream.h
//  CSocket
//
//  Created by Наиль  on 27.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

typedef NS_ENUM(NSInteger, CState) {
    
    CState_None = 0,
    CState_Opening,
    CState_Closing,
    CState_Open,
    CState_Close
};

@protocol CIOSteamDeleage <NSStreamDelegate>

- (void) stream:(NSStream *)aStream didWriteData:(NSData *)data;

@end

@interface CIOStream : NSObject

@property (nonatomic, assign) id<CIOSteamDeleage> delegate;

@property (nonatomic, assign, readonly) CState state;

+ (id) streamWithHandshakeURL:(NSURL *)url;

- (size_t) writeData:(NSData *)data withError:(NSError * __autoreleasing *)error;

- (NSData *) readWithError:(NSError * __autoreleasing *)error;

- (void) open;

- (void) close;


@end
