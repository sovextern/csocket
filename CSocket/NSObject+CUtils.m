//
//  NSObject+CUtils.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+CUtils.h"

@implementation NSObject(CUtils)

- (id) objectAs:(Class)cls
{
    return [self isKindOfClass:cls] ? self : nil;
}

@end
