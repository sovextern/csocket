//
//  NSString+CUtils.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

@interface NSString(CUtils)

+ (NSString *) randomKeyWithSize:(size_t)size;

@end
