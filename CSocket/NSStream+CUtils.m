//
//  NSStream+CUtils.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *const kStreamErrorKey = @"SrteamErrorKey";

@implementation NSStream(CUtils)

- (NSError *) failWithErrorCode:(NSInteger)code andDescription:(NSString *)description
{
    NSError *streamError = self.streamError;
    NSMutableDictionary *mDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:description,NSLocalizedDescriptionKey, nil];
    
    if (streamError != nil)
    {
        [mDict addEntriesFromDictionary:@{kStreamErrorKey:streamError}];
    }
    
    return [NSError errorWithDomain:NSPOSIXErrorDomain code:code userInfo:[mDict copy]];
}

- (size_t) getDefaultSize
{
    static size_t size;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        size = getpagesize();
    });
    return size;
}


@end
