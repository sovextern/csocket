//
//  CStream.m
//  CSocket
//
//  Created by Наиль  on 27.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#include <CoreFoundation/CoreFoundation.h>
#import <Security/Security.h>

#import "CIOStream.h"
#import "CSecurity.h"

#import "NSURL+CUtils.h"
#import "NSStream+CUtils.h"
#import "NSString+CUtils.h"



@interface CIOStream()<NSStreamDelegate>

@property (nonatomic, strong) NSURL *handshakeURL;

@property (nonatomic, strong) NSInputStream *readStream;

@property (nonatomic, strong) NSOutputStream *writeStream;

@property (nonatomic, strong) CSecurity *policy;

@property (nonatomic, assign) CState state;

@property (nonatomic, strong) dispatch_queue_t workQueue;

@end

@implementation CIOStream

- (instancetype) initWithHandshakeURL:(NSURL *)handshake
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.handshakeURL = handshake;
    self.workQueue = dispatch_queue_create(NULL, DISPATCH_QUEUE_SERIAL);
    BOOL isSequre = [self.handshakeURL isSecureQuery];
    
    self.policy = [[CSecurity alloc] initWithRequireSSL:isSequre];
    [self commonInit];
    return self;
}

- (void) commonInit
{
    NSString *hostname = self.handshakeURL.host;
    
    uint32_t port = 80;
    
    if (!self.handshakeURL.port)
    {
        port = 80;
        
        if ([self.handshakeURL isSecureQuery])
        {
            port = 443;
        }
    } else {
        port = [self.handshakeURL.port unsignedIntValue];
    }
    
    CFReadStreamRef _readStreamRef;
    CFWriteStreamRef _writeStreamRef;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)hostname, (uint32_t)port, &_readStreamRef, &_writeStreamRef);

    _writeStream = CFBridgingRelease(_writeStreamRef);
    _readStream  = CFBridgingRelease(_readStreamRef);
    
    _readStream.delegate = self;
    _writeStream.delegate = self;
}

#pragma mark - NSStreamDelegate methods

- (void) stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
    if (self.state == CState_Close)
    {
        return;
    }
    
    if (self.state == CState_Open)
    {
        [self.delegate stream:aStream handleEvent:eventCode];
        return;
    }
    
    NSError *error = nil;
    NSData *data = nil;
    switch (eventCode)
    {
        case NSStreamEventHasSpaceAvailable:
            if (self.state == CState_Opening)
            {
                return;
            }
            
            if (! [self didConnect])
            {
                self.state = CState_Close;
                [self stream:aStream handleEvent:NSStreamEventErrorOccurred];
                return;
            }
        
            break;
        case NSStreamEventHasBytesAvailable:
            data = [self readWithError:&error];
            
            if (error)
            {
                self.state = CState_Close;
                [self stream:aStream handleEvent:NSStreamEventErrorOccurred];
            }
            
            [self renederResponseHeadrsFromData:data];

            break;
        case NSStreamEventErrorOccurred:
            self.state = CState_Close;
            [self stream:aStream handleEvent:NSStreamEventErrorOccurred];
            break;
        default:
            break;
    }
}

#pragma mark - Publcik API methods

+ (id) streamWithHandshakeURL:(NSURL *)url
{
    return [[CIOStream alloc] initWithHandshakeURL:url];
}

- (NSData *) readWithError:(NSError * __autoreleasing *)error
{
    if (!_readStream || !_readStream.hasBytesAvailable)
    {
        return nil;
    }
    
    NSMutableData *mData = [[NSMutableData alloc] init];
    size_t size = 2048;

    uint8_t buffer[size];
    NSInteger step = 0;
    
    while (_readStream.hasBytesAvailable)
    {
        NSUInteger readLenght = [_readStream read:buffer maxLength:size];
        if (readLenght == -1)
        {
            *error = [_readStream failWithErrorCode:1112 andDescription:@"Unable to read from input stream"];
            return nil;
        }
        
        NSData *data = [NSData dataWithBytes:buffer length:readLenght];
        
        if (data)
        {
            [mData appendData:data];
        }
        
        step +=1;
    }
 
    return [mData copy];
}

- (size_t) writeData:(NSData *)data withError:(NSError * __autoreleasing *)error
{
    if (!_writeStream && !_writeStream.hasSpaceAvailable)
    {
        return -1;
    }
    
    
    if (data == nil)
    {
        *error = [_writeStream failWithErrorCode:1110 andDescription:@"Subdata is nil"];
        return NO;
    }
    
    NSUInteger dataLenght = data.length;
    
    const uint8_t *dataBytes = [data bytes];
    NSInteger result = [_writeStream write:dataBytes maxLength:dataLenght];
    
    if (result == -1)
    {
        *error = [_writeStream failWithErrorCode:1116 andDescription:@"Error write to stream"];
        return NO;
    }
    
    return result;
}

- (void) open
{
    [self.policy updateSecuritySettingsForStream:_writeStream];
    [self.policy updateSecuritySettingsForStream:_readStream];
    
    [self.readStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.writeStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

    [self.readStream open];
    [self.writeStream open];
}

- (void) close
{
    self.readStream.delegate = nil;
    self.writeStream.delegate = nil;
    
    [self.readStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.writeStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
   
    [self.writeStream close];
    [self.readStream close];
    
    self.writeStream = nil;
    self.readStream = nil;
    
    self.state = CState_Close;
}

#pragma mark - utils methods

- (void) renederResponseHeadrsFromData:(NSData *)data
{
    if (data == nil)
    {
        return;
    }
    
    NSString *responseHeaders = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    CFHTTPMessageRef responseMessage = CFHTTPMessageCreateEmpty(NULL, NO);
    CFHTTPMessageAppendBytes(responseMessage, data.bytes, data.length);
    
    NSInteger responseCode = CFHTTPMessageGetResponseStatusCode(responseMessage);
    
    if (responseCode != 101)
    {
        NSLog(@"%@ Failed to open socket stream -> response headers: %@",NSStringFromClass(self.class),responseHeaders);
        [self stream:self.readStream handleEvent:NSStreamEventErrorOccurred];
        CFRelease(responseMessage);
        return;
    }
    
    self.state = CState_Open;
    NSLog(@"Successfull open socket: %@",responseHeaders);
    [self stream:self.writeStream handleEvent:NSStreamEventOpenCompleted];
    CFRelease(responseMessage);
}

- (BOOL) didConnect;
{
    NSString *host = [self.handshakeURL validateHost];
    NSString *urlString = [self.handshakeURL validateUrlString];
    NSURL *url = [NSURL URLWithString:urlString];
    
    CFHTTPMessageRef message = CFHTTPMessageCreateRequest(kCFAllocatorDefault, CFSTR("GET"), (__bridge CFURLRef)url, kCFHTTPVersion1_1);
    NSString *key = [NSString randomKeyWithSize:16];
    NSString *origin = [self.handshakeURL origin];
    
    if (!key)
    {
        return NO;
    }
    
    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Host"), (__bridge CFStringRef)host);
    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Upgrade"), CFSTR("websocket"));
    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Connection"), CFSTR("Upgrade"));
    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Sec-WebSocket-Version"),(__bridge CFStringRef)@(13).stringValue);
    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Sec-WebSocket-Key"), (__bridge CFStringRef)key);
    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Origin"), (__bridge CFStringRef)origin);
    
    CFDataRef messageData = CFHTTPMessageCopySerializedMessage(message);
    NSData *dataToSend = (__bridge_transfer NSData *)messageData;
    NSError *error = nil;
    size_t size = [self writeData:dataToSend withError:&error];
    
    if (size == -1 || error)
    {
        return NO;
    }
    
    CFRelease(message);
    self.state = CState_Opening;
    return YES;
}
@end
