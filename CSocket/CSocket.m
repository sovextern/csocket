//
//  CSocket.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <CFNetwork/CFNetwork.h>

#import "CSocket.h"
#import "CIOStream.h"
#import "CParser.h"
#import "CFrame.h"

#import "NSObject+CUtils.h"
#import "NSString+CUtils.h"
#import "NSURL+CUtils.h"

typedef struct {
    
    unsigned int didSendData;
    unsigned int didRecvData;
    unsigned int didOpen;
    unsigned int didClose;
    unsigned int didCloseWithError;

} DelegateFlags;


@interface CSocket()<CIOSteamDeleage,CParserDelegate>
{
    dispatch_queue_t _workQueue;
    DelegateFlags _delegateFlags;
}

@property (nonatomic, strong) NSURL *url;

@property (nonatomic, strong) CParser *parser;

@property (nonatomic, strong) CIOStream *stream;

@property (nonatomic, strong) NSMutableArray *dataToWrite;

@end

@implementation CSocket

#pragma mark - init methods

- (instancetype) initWithURLString:(NSString *)urlString;
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    
    self.stream = nil;
    
    const char *queueLabel = NSStringFromClass(self.class).UTF8String;
    _workQueue = dispatch_queue_create(queueLabel, DISPATCH_QUEUE_CONCURRENT);
    _url = [NSURL URLWithString:urlString];
    _dataToWrite = [NSMutableArray new];
    
    dispatch_queue_set_specific(_workQueue, (__bridge void *)self, (__bridge void *)(_workQueue), NULL);
    
    self.parser = [[CParser alloc] init];
    self.parser.delegate = self;
    
    return self;
}

#pragma mark - Publick API methods

- (void) setDelegate:(id<CSocketDelegate>)delegate
{
    _delegate = delegate;
    
    _delegateFlags.didOpen = [delegate respondsToSelector:@selector(socketDidOpen:)];
    _delegateFlags.didClose = [delegate respondsToSelector:@selector(socketDidClose:)];
    _delegateFlags.didCloseWithError = [delegate respondsToSelector:@selector(socket:didClosedWithError:)];
    _delegateFlags.didSendData = [delegate respondsToSelector:@selector(socket:didSendData:)];
    _delegateFlags.didRecvData = [delegate respondsToSelector:@selector(socket:didReciveData:)];
}

- (BOOL) connect
{
    self.stream = [CIOStream streamWithHandshakeURL:self.url];
    self.stream.delegate = self;
    
    [self.stream open];
    return YES;
}

- (BOOL) disconnect
{
    [self.stream close];
    
    if (_delegateFlags.didClose)
    {
        [self.delegate socketDidClose:self];
    }
    
    return YES;
}

- (BOOL) sendData:(NSData *)data
{
    if (data == nil)
    {
        return NO;
    }
    return  [self writeData:data];;
}

- (BOOL) sendJSONDictionary:(NSDictionary *)dict
{
    NSData *data = [self.parser parseDataFromJSONDictionary:dict];
    
    if (!data)
    {
        return NO;
    }
    
    return [self writeData:data];
}


#pragma mark - NSStreamDelegate protocols

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
    switch (eventCode) {
        case NSStreamEventOpenCompleted:
            
            if (_delegateFlags.didOpen)
            {
                [self.delegate socketDidOpen:self];
            }
            [self write];
            [self read];
            break;
        case NSStreamEventErrorOccurred:
             NSLog(@"Socket close");
            [self finishWithError:aStream.streamError];
            break;
        case NSStreamEventEndEncountered:
            NSLog(@"Socket close");
            [self disconnect];
        case NSStreamEventHasBytesAvailable:
            [self read];
            break;
        case NSStreamEventHasSpaceAvailable:
            [self write];
            break;
        default:
            break;
    }
}

- (void) stream:(NSStream *)aStream didWriteData:(NSData *)data
{
    if (_delegateFlags.didSendData)
    {
        [self.delegate socket:self didSendData:data];
    }
}

#pragma mark - utils methods

- (BOOL) writeData:(NSData *)data
{
    NSData *rfcData = [self.parser parseDataToRFCStandartFromData:data withCode:RFCOpcode_TEXT];
    NSError *error = nil;
    
    if (self.stream.state == CState_Close)
    {
        [self.stream open];
        return NO;
    }
    
    if (error)
    {
        [self finishWithError:error];
        return NO;
    }
    
    if (self.stream.state != CState_Open)
    {
        NSUInteger index = [self.dataToWrite indexOfObject:rfcData];
        
        if (index != NSNotFound)
        {
            return NO;
        }
        
        @synchronized (self)
        {
            [self.dataToWrite addObject:rfcData];
        }
        
        return self.stream.state == CState_Opening;
    }
    
    return [self.stream writeData:rfcData withError:&error];
}

- (void) write
{
    __weak typeof(self) weakSelf = self;
   // dispatch_async(_workQueue, ^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        NSError *error = nil;
        
        @synchronized (blockSelf)
        {
            NSMutableArray *dataToRemove = [NSMutableArray new];
            for (NSMutableData *mData in blockSelf.dataToWrite)
            {
                size_t dataLenght = mData.length;
                size_t size = [blockSelf.stream writeData:mData withError:&error];
                
                if (error || size == -1)
                {
                    if (error)
                    {
                        [blockSelf finishWithError:error];
                        return;
                    }
                    return;
                }
                
                if (dataLenght > size)
                {
                    NSData *subData = [mData subdataWithRange:NSMakeRange(size, dataLenght-size)];
                    [mData setData:subData];
                    return;
                }
                
                
                [dataToRemove addObject:mData];
            }
            [blockSelf willChangeValueForKey:@"dataToWrite"];
            [blockSelf.dataToWrite removeObjectsInArray:dataToRemove];
            [blockSelf didChangeValueForKey:@"dataToWrite"];
        }
        
        NSLog(@"%@ successfull write",NSStringFromClass(self.class));
   // });
}

- (void) read
{
    __weak typeof(self) weakSelf = self;
  //  dispatch_async(_workQueue, ^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        NSError *error = nil;
        NSData *data = [blockSelf.stream readWithError:&error];
        
        if (error || !data)
        {
            if (error)
            {
                [blockSelf finishWithError:error];
                return;
            }
            return;
        }
    
       NSLog(@"%@ Recived data: %ld",NSStringFromClass(self.class),(int)data.length);
    
        [self.parser parseDataFromRFSStandart:data];
  //  });
}

- (void) finishWithError:(NSError *)error
{
    [self.stream close];
    
    if (_delegateFlags.didCloseWithError)
    {
        [self.delegate socket:self didClosedWithError:error];
    }
}

#pragma mark - CParserDelegate protocol methods

- (void) didCompleteParseFrame:(CFrame *)frame
{
    NSData *data = [frame componentsJoined];
    
    if (_delegateFlags.didRecvData)
    {
        [self.delegate socket:self didReciveData:data];
    }
}

@end
