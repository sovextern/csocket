
//
//  NSURL+CUtils.m
//  CSocket
//
//  Created by Наиль  on 26.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSURL+CUtils.h"

@implementation NSURL(CUtils)

- (NSString *) origin
{
    NSMutableString *origin = [NSMutableString string];
    
    NSString *scheme = self.scheme.lowercaseString;
    if ([scheme isEqualToString:@"wss"]) {
        scheme = @"https";
    } else if ([scheme isEqualToString:@"ws"]) {
        scheme = @"http";
    }
    [origin appendFormat:@"%@://%@", scheme, self.host];
    
    NSNumber *port = self.port;
    BOOL portIsDefault = (!port ||
                          ([scheme isEqualToString:@"http"] && port.integerValue == 80) ||
                          ([scheme isEqualToString:@"https"] && port.integerValue == 443));
    if (!portIsDefault) {
        [origin appendFormat:@":%@", port.stringValue];
    }
    return origin;
}

- (NSString *) validateUrlString
{
    NSString *scheme = self.scheme.lowercaseString;
    
    if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"])
    {
        return self.absoluteString;
    }
    
    if ([scheme isEqualToString:@"wss"]) {
        scheme = @"https";
    } else if ([scheme isEqualToString:@"ws"]) {
        scheme = @"http";
    }
    
    return [NSString stringWithFormat:@"%@://%@%@",scheme,self.host,self.path];
}

- (NSString *) validateHost
{
    NSString *host = self.host;
    if (self.port)
    {
        host = [host stringByAppendingFormat:@":%@", self.port];
    }
    return host;
}


- (BOOL) isSecureQuery
{
    NSString *scheme = self.scheme.lowercaseString;
    
    if ([scheme isEqualToString:@"wss"]) {
        return YES;
    }
    if ([scheme isEqualToString:@"ws"]) {
        return NO;
    }
    
    if ([scheme isEqualToString:@"https"])
    {
        return YES;
    }
    
    if ([scheme isEqualToString:@"http"])
    {
        return NO;
    }
    
    return NO;
}

@end
