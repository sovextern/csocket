//
//  NSURL+CUtils.h
//  CSocket
//
//  Created by Наиль  on 26.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

@interface NSURL(CUtils)

- (NSString *) validateUrlString;
- (NSString *) validateHost;
- (NSString *) origin;

- (BOOL) isSecureQuery;

@end
