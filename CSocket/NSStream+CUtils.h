//
//  NSStream+CUtils.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSStream(CUtils)

- (NSError *) failWithErrorCode:(NSInteger)code andDescription:(NSString *)description;

- (size_t) getDefaultSize;

@end
