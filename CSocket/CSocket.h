//
//  CSocket.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

@class CSocket;

typedef void (^CSocketCompletionBlock)(BOOL success);

@protocol CSocketDelegate <NSObject>

- (void) socket:(CSocket *)socket didReciveData:(NSData *)data;

- (void) socket:(CSocket *)socket didSendData:(NSData *)data;

- (void) socket:(CSocket *)socket didClosedWithError:(NSError *)error;

- (void) socketDidClose:(CSocket *)socket;

- (void) socketDidOpen:(CSocket *)socket;

@end

@interface CSocket : NSObject

@property (nonatomic, weak) id<CSocketDelegate> delegate;

- (instancetype) initWithURLString:(NSString *)urlString;

- (BOOL) connect;

- (BOOL) disconnect;

- (BOOL) sendJSONDictionary:(NSDictionary *)dict;

- (BOOL) sendData:(NSData *)data;

@end
