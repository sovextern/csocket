//
//  CParser.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>


#import "CParser.h"

#import "CFrame.h"

static const size_t CRFCOverhead = 32;

static const uint8_t RFCFinMask         = 0x80;
static const uint8_t RFCFinMasked       = 0x80;
static const uint8_t RFCOpCodeMask      = 0x0F;
static const uint8_t RFCPayloadLenMask  = 0x7F;

typedef struct {
    BOOL fin;
    uint8_t opcode;
    BOOL masked;
    uint64_t payload_length;
} rfcFrame;

typedef uint8_t uint8x32_t __attribute__((vector_size(32)));

void CPMaskBytesManual(uint8_t *bytes, size_t length, uint8_t *maskKey);
void CPMaskBytesSIMD(uint8_t *bytes, size_t length, uint8_t *maskKey);

@interface CParser()

@property (nonatomic, strong) NSMutableData *restData;

@property (nonatomic, strong) NSMutableArray *frames;

@property (nonatomic, strong) CFrame *currentFrame;



@end

@implementation CParser

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
   return self;
}

- (NSData *) parseDataFromJSONDictionary:(NSDictionary *)dict
{
    if (!dict)
    {
        return nil;
    }
    
#warning TODO write JSON check
    
    return [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
}

- (NSData *) parseDataToRFCStandartFromData:(NSData *)data withCode:(RFCOpcode)code
{
    if (!data)
    {
        return nil;
    }
    
    NSMutableData *frameData = [[NSMutableData alloc] initWithLength:data.length+CRFCOverhead];
    uint8_t *mutableBytes = (uint8_t *)frameData.mutableBytes;
    
    mutableBytes[0] = RFCFinMask | code;
    
    mutableBytes[1] |= RFCFinMasked;
    
    size_t payloadLength = data.length;
    size_t bytesSize = 2;
    
    if (payloadLength < 126)
    {
        mutableBytes[1] |= payloadLength;
    } else {
        uint64_t declaredPayloadLength = 0;
        size_t declaredPayloadLengthSize = 0;
        
        if (payloadLength <= UINT16_MAX) {
            mutableBytes[1] |= 126;
            
            declaredPayloadLength = CFSwapInt16BigToHost((uint16_t)payloadLength);
            declaredPayloadLengthSize = sizeof(uint16_t);
        } else {
            mutableBytes[1] |= 127;
            
            declaredPayloadLength = CFSwapInt64BigToHost((uint64_t)payloadLength);
            declaredPayloadLengthSize = sizeof(uint64_t);
        }
        
         memcpy((mutableBytes+bytesSize), &declaredPayloadLength, declaredPayloadLengthSize);
        bytesSize += declaredPayloadLengthSize;
    }
    
    const uint8_t *unmaskedPayloadBuffer = (uint8_t *)data.bytes;
    uint8_t *maskKey = mutableBytes + bytesSize;
    
    size_t randomBytesSize = sizeof(uint32_t);
    SecRandomCopyBytes(kSecRandomDefault, randomBytesSize, maskKey);
    
    bytesSize += randomBytesSize;
    
    // Copy and unmask the buffer
    uint8_t *frameBufferPayloadPointer = mutableBytes + bytesSize;
    
    memcpy(frameBufferPayloadPointer, unmaskedPayloadBuffer, payloadLength);
    CPMaskBytesManual(frameBufferPayloadPointer, payloadLength, maskKey);
    bytesSize += payloadLength;
    frameData.length = bytesSize;

    return [frameData copy];
}

- (void) parseDataFromRFSStandart:(NSData *)data
{
    if (!data || data.length == 0)
    {
        return;
    }
    
    CFrame *frame = self.currentFrame;
    
    if (!frame)
    {
        frame = [self createNewFrameWithData:data];
        
        if (frame.frameType != CFrameType_TEXT)
        {
            [self completeParseFrame:frame];
            return;
        }
        
        if (!frame.filled || !frame.last)
        {
            self.currentFrame = frame;
            
            [self continueReadIfNeededDataPack:data];
            return;
        }
    }
    
    if (frame.filled && !frame.last)
    {
        frame = [self createNewFrameWithData:data];
        
        frame.prevFrame = self.currentFrame;
        self.currentFrame = frame;
        
        if (frame.filled && frame.last)
        {
            self.currentFrame = nil;
            [self completeParseFrame:frame];
        }
        
        [self continueReadIfNeededDataPack:data];
        return;
    }
    
    uint64_t needBytes = frame.payload_lenght - frame.data.length;
    uint64_t curSize = data.length;
    
    if (curSize >= needBytes)
    {
        [frame.data appendData:[data subdataWithRange:NSMakeRange(0, (NSUInteger)needBytes)]];
        frame.filled = YES;
        
        if (frame.last)
        {
            self.currentFrame = nil;
            [self completeParseFrame:frame];
        }
    }
    
    [frame.data appendData:data];
}


#pragma mark - utils methods

- (void) continueReadIfNeededDataPack:(NSData *)data
{
    CFrame *frame = self.currentFrame;
    
    NSData *frameData = frame.data;
    
    if (frameData.length + frame.offset < data.length )
    {
        NSInteger offset = frameData.length + frame.offset;
        NSData *subData = [data subdataWithRange:NSMakeRange(offset, data.length-offset)];
        [self parseDataFromRFSStandart:subData];
    }
    
    return;
}

- (CFrame *) createNewFrameWithData:(NSData *)data
{
    CFrame *frame = [[CFrame alloc] init];
    
    uint8_t *bytes = (uint8_t *)data.bytes;
    uint8_t receivedOpCode = (RFCOpCodeMask & bytes[0]);
    
    BOOL isControl = (receivedOpCode == RFCOpCode_PING || receivedOpCode == RFCOpCode_CONNECTION_CLOSE || receivedOpCode == RFCOpCode_PONG);
    
    if (isControl)
    {
        NSLog(@"Controll opcode");
        return frame;
    }
    
    uint64_t payloadLength = RFCPayloadLenMask&bytes[1];
    size_t bytesOffset = 2;
    size_t extra_bytes_needed = 0;
    
    frame.last = !!(RFCFinMask & bytes[0]);
    frame.filled = NO;
    
    if ( payloadLength == 126 )
    {
        extra_bytes_needed += sizeof(uint16_t);
        const void *mapped_buffer = [data subdataWithRange:NSMakeRange(bytesOffset, extra_bytes_needed)].bytes;
        memcpy(&payloadLength, mapped_buffer, sizeof(uint16_t));
        payloadLength = CFSwapInt16BigToHost(payloadLength);
        
        frame.payload_lenght = payloadLength;
    }
    else if ( payloadLength == 127 )
    {
        extra_bytes_needed += sizeof(uint64_t);
        const void *mapped_buffer = [data subdataWithRange:NSMakeRange(bytesOffset, extra_bytes_needed)].bytes;
        memcpy(&payloadLength, mapped_buffer, sizeof(uint64_t));
        payloadLength = CFSwapInt64BigToHost(payloadLength);
        
        frame.payload_lenght = payloadLength;
    }
    else
    {
       frame.payload_lenght = payloadLength;
    }
    
    frame.frameType =  CFrameType_TEXT;
    
    
    uint64_t needBytes = frame.payload_lenght;
    uint64_t curSize = data.length - (extra_bytes_needed + bytesOffset);
    
    if (curSize >= needBytes)
    {
        curSize = needBytes;
        frame.filled = YES;
    }
    
    frame.offset = extra_bytes_needed + bytesOffset;
    [frame.data appendData:[data subdataWithRange:NSMakeRange(extra_bytes_needed + bytesOffset, (NSUInteger)curSize)]];
    
    if (frame.last && frame.filled && !self.currentFrame)
    {
        [self completeParseFrame:frame];
    }

    return frame;
}

- (size_t) getDefaultSize
{
    static size_t size;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        size = getpagesize();
    });
    return size;
}

- (void) completeParseFrame:(CFrame *)frame
{
    if ([self.delegate respondsToSelector:@selector(didCompleteParseFrame:)])
    {
        [self.delegate didCompleteParseFrame:frame];
    }
}


@end

void CPMaskBytesManual(uint8_t *bytes, size_t length, uint8_t *maskKey) {
    for (size_t i = 0; i < length; i++) {
        bytes[i] = bytes[i] ^ maskKey[i % sizeof(uint32_t)];
    }
}




