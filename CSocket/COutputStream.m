

//
//  COutputStream.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "COutputStream.h"

#import "NSStream+CUtils.h"

@interface COutputStream()
{
    NSOutputStream *_outputStream;
}

@property (nonatomic, strong) NSMutableArray *dataToWrite;
@property (nonatomic, assign) NSInteger dataOffset;


@end

@implementation COutputStream

#pragma mark - init methods

- (instancetype) initWithStream:(NSOutputStream *)outputStream
{
    if ( ( self = [super init]) == nil)
    {
        return nil;
    }
    _outputStream = outputStream;
    
    return self;
}

+ (id) outputStreamFromStream:(NSOutputStream *)outputStream
{
    return [[COutputStream alloc] initWithStream:outputStream];
}

#pragma mark - ovverides

- (BOOL) isEqual:(id)object
{
    return [_outputStream isEqual:object];
}


#pragma mark - Publick API methods

- (void) setDelegate:(id<NSStreamDelegate>)delegate
{
    _outputStream.delegate = delegate;
}

- (id<NSStreamDelegate>) delegate
{
    return _outputStream.delegate;
}

- (BOOL) writeData:(NSData *)data withError:(NSError * __autoreleasing *)error
{
    if (!self.dataToWrite)
    {
        self.dataToWrite = [[NSMutableArray alloc] initWithObjects:data,nil];
    } else {
        [self.dataToWrite addObject:data];
    }
    
    return [self writeSwapWithError:error];
}

- (BOOL) writeSwapWithError:(NSError * __autoreleasing *)error
{
    if (!_outputStream.hasSpaceAvailable)
    {
        return NO;
    }
    
    if (_dataToWrite.count == 0)
    {
        return NO;
    }
    @synchronized (self) {
    
        NSMutableArray *_dataToRemove = [[NSMutableArray alloc] init];

        for (NSMutableData *data in _dataToWrite)
        {
            if (data == nil)
            {
                *error = [_outputStream failWithErrorCode:1110 andDescription:@"Subdata is nil"];
                return NO;
            }
            
            NSUInteger dataLenght = data.length;

            const uint8_t *dataBytes = [data bytes];
            NSInteger result = [_outputStream write:dataBytes maxLength:dataLenght];
            
            if (result == -1)
            {
                *error = [_outputStream failWithErrorCode:1116 andDescription:@"Error write to stream"];
                return NO;
            }
            
            if (result < dataLenght)
            {
                [data setData:[data subdataWithRange:NSMakeRange(result, dataLenght)]];
                break;
            }
            
            [_dataToRemove addObject:data];
        }
        
        [_dataToWrite removeObjectsInArray:_dataToRemove];
    }
    
    return YES;
}

- (void) open
{
    [_outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_outputStream open];
}

- (void) close
{
    _outputStream.delegate = nil;
    [_outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_outputStream close];
}

#pragma mark - utils methods



@end
