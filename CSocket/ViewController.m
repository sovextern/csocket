//
//  ViewController.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import "ViewController.h"
#import "CSocket.h"

typedef struct {
    
    const char *text;
    const char *from;
    const char *to;
    int64_t date;

} Example;

@interface ViewController ()

@property (nonatomic, strong) CSocket *test;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _test = [[CSocket alloc] initWithURLString:@"ws://10.64.61.176:3001/ws"];
    
    NSDictionary *dict = @{
                           @"os":@"iOS",
                           @"device_token":@"sadasdasdas",
                           @"phone":@"+79673714009",
                           @"is_staff":@(NO) };
    [_test connect];
    [_test sendJSONDictionary:dict];
  
    NSDictionary *dict1 = @{
                                @"text":@"safsadfsdfgsdf",
                                @"from":@"+79673714009",
                                @"to":@"Staff",
                                @"date":@((uint64_t)[[NSDate date] timeIntervalSince1970])
                           };
    
    [_test sendJSONDictionary:dict1];
    
    NSURLSession *urlSession = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://crcal-test.sovcombank.ru/chat/ws"]];
    
    urlRequest.allHTTPHeaderFields = @{
                                        @"Upgrade":@"websocket",
                                        @"Connection":@"Upgrade",
                                        @"Sec-WebSocket-Key":@"pv4YNlY5sUSKqRSalM+JIg==",
                                        @"Sec-WebSocket-Version":@"13",
                                        @"Host":@"crcal-test.sovcombank.ru"
                                      };
    
//    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Host"), (__bridge CFStringRef)host);
//    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Upgrade"), CFSTR("websocket"));
//    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Connection"), CFSTR("Upgrade"));
//    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Sec-WebSocket-Version"),(__bridge CFStringRef)@(13).stringValue);
//    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Sec-WebSocket-Key"), (__bridge CFStringRef)key);
//    CFHTTPMessageSetHeaderFieldValue(message, CFSTR("Origin"), (__bridge CFStringRef)origin);
//    
    
    NSURLSessionDataTask *dataTask = [urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
    }];
    
    
    
    [dataTask resume];
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
