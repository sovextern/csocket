//
//  CInputStream.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CInputStream.h"

#import "NSStream+CUtils.h"

@interface CInputStream()
{
    NSInputStream * _inputStream;
}

@property (nonatomic, assign) NSInteger readOffset;
@property (nonatomic, strong) NSData *lastReadData;


@end

@implementation CInputStream

#pragma mark - init methods

- (instancetype) initWithInputStream:(NSInputStream *)inputStream
{
    if ( ( self = [super init]) == nil )
    {
        return nil;
    }
    _inputStream = inputStream;
    return self;
}

+ (id) inputStreamFromStream:(NSInputStream *)inputStream
{
    return [[CInputStream alloc] initWithInputStream:inputStream];
}

#pragma mark - Publick API methods

- (void) setDelegate:(id<NSStreamDelegate>)delegate
{
    _inputStream.delegate = delegate;
}

- (id<NSStreamDelegate>) delegate
{
    return _inputStream.delegate;
}

- (void) open
{
    [_inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_inputStream open];
}

- (void) close
{
    _inputStream.delegate = nil;
    [_inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_inputStream close];
}


@end
