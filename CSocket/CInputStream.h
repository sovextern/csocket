//
//  CInputStream.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CInputStream : NSObject

@property (nonatomic, assign) id<NSStreamDelegate> delegate;

+ (id) inputStreamFromStream:(NSInputStream *)inputStream;

- (NSData *) readWithError:(NSError * __autoreleasing *)error;

- (void) open;

- (void) close;

@end
