//
//  COutputStream.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface COutputStream : NSObject

@property (nonatomic, assign) id<NSStreamDelegate> delegate;

+(id) outputStreamFromStream:(NSOutputStream *)outputStream;

- (void) open;

- (void) close;

- (BOOL) writeData:(NSData *)data withError:(NSError * __autoreleasing *)error;

- (BOOL) writeSwapWithError:(NSError * __autoreleasing *)error;

@end
