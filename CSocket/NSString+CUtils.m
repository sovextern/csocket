
//
//  NSString+CUtils.m
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+CUtils.h"

@implementation NSString(CUtils)

+ (NSString *) randomKeyWithSize:(size_t)size
{
    uint8_t *nonce = malloc(size);
    int res = SecRandomCopyBytes(kSecRandomDefault, 16, nonce);
    
    if (res == -1)
    {
        return nil;
    }
    
    NSData *data = [NSData dataWithBytes:nonce length:size];
    free(nonce);
    
    return [data base64EncodedStringWithOptions:0];
}

@end
