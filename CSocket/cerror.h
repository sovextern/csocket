//
//  cerror.h
//  CSocket
//
//  Created by Наиль  on 25.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

class cerror
{
public:
    cerror(int error_code,const char *error_desc);
    cerror(int error_code);
    cerror(const char *error_desc);
    
    const char* formmatedError();
private:
    int error_code;
    const char *error_desc;
};
