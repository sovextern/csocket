//
//  CSecurity.h
//  CSocket
//
//  Created by Наиль  on 26.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

@interface CSecurity : NSObject

- (instancetype) initWithRequireSSL:(BOOL)requireSSL;

- (void) updateSecuritySettingsForStream:(NSStream *)stream;


@end
