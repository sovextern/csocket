//
//  CSecurity.m
//  CSocket
//
//  Created by Наиль  on 26.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSecurity.h"

@interface CSecurity()

@property (nonatomic, assign) BOOL requireSSL;

@end

@implementation CSecurity

- (instancetype) initWithRequireSSL:(BOOL)requireSSL
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.requireSSL = requireSSL;
    return self;
}

- (void) updateSecuritySettingsForStream:(NSStream *)stream
{
    if (!self.requireSSL)
    {
        return;
    }
    
    [stream setProperty:(__bridge id)CFSTR("kCFStreamSocketSecurityLevelTLSv1_2") forKey:(__bridge id)kCFStreamPropertySocketSecurityLevel];
    
    // Validate certificate chain for this stream if enabled.
    NSDictionary<NSString *, id> *sslOptions = @{ (__bridge NSString *)kCFStreamSSLValidatesCertificateChain : @(YES) };
    
    [stream setProperty:sslOptions forKey:(__bridge NSString *)kCFStreamPropertySSLSettings];
}

@end
